# Journal Article  
## Title: **The Indirect Cost of E-Mobility: Global Warming Impact of Grid Energy-Mix on E-Mobility Using Real-World Data**

### Abstract

Electric vehicles are considered by many an emissions-free or low-emissions solution to meeting the challenge of sustainable transportation. However, the operational input, electrical energy, has an associated cost, greenhouse gasses, which results in indirect emissions. The goal of this paper is to provide an assessment of the impact of a grid's energy mix on the indirect emissions of an electric vehicle. The study considers real-world data, vehicle usage data from an electric vehicle, and carbon intensity data for India, the USA, France, the Netherlands, Brazil, Germany, and Poland. A linear programming-based optimization is used to compute the best-case scenario for each of the given grids and, consequently, the indirect emissions were compared to that of a high-efficiency internal combustion engine vehicle (a Renault Clio). The results indicate that for grids with a low renewable energy penetration such as those of Poland and India(Maharashtra), an electric vehicle can be classified as neither a low nor zero-emissions alternative to normal thermal vehicles. Additionally, for grids with high levels of variation in their carbon intensity, there exists a significant potential to reduce the carbon footprint related to the charging of an electric vehicle. This paper provides a real-world perspective of how an electric vehicle performed in the face of different energy mixes and serves as a precursor to the development of robust indicators for determining the carbon reductions related to the e-mobility transition.


## Authors
This notebook was prepared by :
1. Nana Kofi Twum-Duah: nana-kofi-baabu.twum-duahd@grenoble-inp.fr 
2. Lucas Hajiro Neves Mosquini: lucas.neves-mosquini@grenoble-inp.fr 
3. Salman Shahid: muhammad-salman.shahid@grenoble-inp.fr 

## Citation


## Requirements
This notebook was prepared on a windows machine with `Python 3.8.8`
to run this notebook, the following libraries are required

* `pandas`Version => 1.3.4
* `numpy` Version => 1.19.5
* `plotly`Version => 5.4.0
* `pyomo` Version == 6.0.1
* `alive-progress` version => 2.4.1

if you want to save the figures using plotly's pio, kindly install plotly orca using 
``` 
conda install -c plotly plotly-orca
```

## Contributing

You can upload your own dat, the subsequent `pandas` dataframe must have the following columns
* **Speed[km/h]**:  vehicle speed in km/h 
* **distance[km]**: the distance traveled in km 
* **Charging_Power[kW]**: Power charged into the vehicle in kW(the dataset here considered the power after losses)
* **Discharged_power[kW]**: Power discharged into the vehicle in kW(the dataset here considered the power after losses)

**Note timestep will depend on your data set**



## License
The notebook and associated data are open-access and are licensed using: 
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)